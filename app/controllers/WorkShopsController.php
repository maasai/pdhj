<?php

class WorkShopsController extends BaseController {

	public function __construct(){
		$this->beforeFilter('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$workshops = WorkShop::all();
		$workshops = WorkShop::paginate(10);
		$this->data = array(
			'workshops' => $workshops,
			'title' => 'WORKSHOPS',
			'menu' => 'WorkShop'
			);
		return View::make('workshops.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('workshops.create')->with('title', 'NEW WORKSHOP');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), WorkShop::$rules);

		if($validator->passes()){
			$workshop = new WorkShop();

			$workshop->start_date = Input::get('start_date');
			$workshop->end_date = Input::get('end_date');
			$workshop->total_hours = Input::get('total_hours');
			$workshop->district = Input::get('district');
			$workshop->sub_district = Input::get('sub_district');
			$workshop->suco = Input::get('suco');
			$workshop->institution = Input::get('institution');
			$workshop->target_group = Input::get('target_group');
			$workshop->general_subject = Input::get('general_subject');
			$workshop->specific_subject = Input::get('specific_subject');
			$workshop->team = Input::get('team');
			$workshop->males = Input::get('males');
			$workshop->females = Input::get('females');
			$workshop->total_participants = Input::get('total_participants');

			$workshop->save();

			return Redirect::to('workshops')
			->with('message', 'Success !! New WorkShop created.');
		}
		return Redirect::to('workshops/create')
		->withErrors($validator)
		->withInput()
		->with('message', 'Please check on the listed errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$workshop = Workshop::find($id);
		if(!is_null($workshop)){
				return View::make('workshops.show');
		}
		return Redirect::to('/')
		->with('message', 'WorkShop is not available. Please Retry.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$workshop = Workshop::find($id);
		if(is_null($workshop))
		{
			return Redirect::to('workshops')->with('message', 'Workshop not available. Please Retry.' );
		}
		$this->data = array(
			'title' => 'EDIT WORKSHOP',
			'workshop' => $workshop
			);

		return View::make('workshops.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$workshop = Workshop::find($id);
		if(!is_null($workshop)){
		//Workshop::$rules['email'] = 'required|email|unique:workshops,email,'.$id;
		$validator = Validator::make(Input::all(), Workshop::$rules);
			if($validator->passes()){
				
			$workshop->start_date = Input::get('start_date');
			$workshop->end_date = Input::get('end_date');
			$workshop->total_hours = Input::get('total_hours');
			$workshop->district = Input::get('district');
			$workshop->sub_district = Input::get('sub_district');
			$workshop->suco = Input::get('suco');
			$workshop->institution = Input::get('institution');
			$workshop->target_group = Input::get('target_group');
			$workshop->general_subject = Input::get('general_subject');
			$workshop->specific_subject = Input::get('specific_subject');
			$workshop->team = Input::get('team');
			$workshop->males = Input::get('males');
			$workshop->females = Input::get('females');
			$workshop->total_participants = Input::get('total_participants');

			$workshop->update();

				return Redirect::to('workshops')
					->with('message', 'Success !! Workshop updated.');
			}
			return Redirect::route('workshops.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('workshops')
			->with('message', 'Workshop not available. Please Retry.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$workshop = Workshop::find($id);
		if(!is_null($workshop)){
			$workshop->delete();
			return Redirect::back()
				->with('Success !! Workshop Deleted.');
		}
		return Redirect::to('/')
			->with('message', 'Workshop not available. Please Retry.');
	}

}