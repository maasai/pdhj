<?php

class SubDistrictsController extends \BaseController {
	public function __construct(){
		$this->beforeFilter('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$subdistricts = SubDistrict::all();
		$subdistricts = SubDistrict::paginate(10);
		$this->data = array(
			'subdistricts' => $subdistricts,
			'title' => 'SUB DISTRICT',
			'menu' => 'Other'
			);
		return View::make('sub-districts.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sub-districts.create')->with('title', 'NEW SUB DISTRICT');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), SubDistrict::$rules);
		if($validator->passes()){
			$subdistrict = new SubDistrict();
			$subdistrict->sub_district_name = Input::get('sub_district_name');
			$subdistrict->save();
			return Redirect::to('sub_districts')
			->with('message', 'Success !! New Sub District Created.');
		}
		return Redirect::to('sub_districts/create')
		->withInput()
		->withErrors($validator)
		->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$subdistrict = SubDistrict::find($id);
		if(!is_null($subdistrict)){
				return View::make('sub-districts.show', $subdistrict);
		}
		return Redirect::to('/')
		->with('message', 'Sub District is not available. Please Retry.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$subdistrict = SubDistrict::find($id);
		if(is_null($subdistrict))
		{
			return Redirect::to('sub_districts')->with('message', 'Sub District not available. Please Retry.' );
		}
		$this->data = array(
			'title' => 'EDIT SUB DISTRICT',
			'subdistrict' => $subdistrict
			);

		return View::make('sub-districts.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$subdistrict = SubDistrict::find($id);
		if(!is_null($subdistrict)){
		$validator = Validator::make(Input::all(), SubDistrict::$rules);
			if($validator->passes()){
				$subdistrict->sub_district_name = Input::get('sub_district_name');
				
				$subdistrict->update();
				return Redirect::to('sub_districts')
					->with('message', 'Success !! Sub District updated.');
			}
			return Redirect::route('sub_districts.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('sub-districts')
			->with('message', 'Sub District not available. Please Retry.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$subdistrict = SubDistrict::find($id);
		if(!is_null($subdistrict)){
			$subdistrict->delete();
			return Redirect::back()
				->with('message', 'Success !! Sub District Deleted.');
		}
		return Redirect::to('/')
			->with('message', 'Sub District not available. Please Retry.');
	}

}