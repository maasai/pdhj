<?php

class SukosController extends BaseController {
	public function __construct(){
		$this->beforeFilter('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sukos = Suko::all();
		$sukos = Suko::paginate(10);
		$this->data = array(
			'sukos' => $sukos,
			'title' => 'SUKO',
			'menu' => 'Other'
			);
		return View::make('sukos.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sukos.create')->with('title', 'NEW SUKO');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Suko::$rules);
		if($validator->passes()){
			$suko = new Suko();
			$suko->suko_name = Input::get('suko_name');
			$suko->save();
			return Redirect::to('sukos')
			->with('message', 'Success !! New Suko Created.');
		}
		return Redirect::to('sukos/create')
		->withInput()
		->withErrors($validator)
		->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$suko = Suko::find($id);
		if(!is_null($suko)){
				return View::make('sukos.show', $suko);
		}
		return Redirect::to('/')
		->with('message', 'Suko is not available. Please Retry.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$suko = Suko::find($id);
		if(is_null($suko))
		{
			return Redirect::to('sukos')->with('message', 'Suko not available. Please Retry.' );
		}
		$this->data = array(
			'title' => 'EDIT SUKO',
			'suko' => $suko
			);

		return View::make('sukos.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$suko = Suko::find($id);
		if(!is_null($suko)){
		$validator = Validator::make(Input::all(), Suko::$rules);
			if($validator->passes()){
				$suko->suko_name = Input::get('suko_name');
				$suko->update();
				return Redirect::to('sukos')
					->with('message', 'Success !! Suko updated.');
			}
			return Redirect::route('sukos.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('sukos')
			->with('message', 'Suko not available. Please Retry.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$suko = Suko::find($id);
		if(!is_null($suko)){
			$suko->delete();
			return Redirect::back()
				->with('message', 'Success !! Suko Deleted.');
		}
		return Redirect::to('/')
			->with('message', 'Suko not available. Please Retry.');
	}

}