<?php

class DistrictsController extends BaseController {
	public function __construct(){
		$this->beforeFilter('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$districts = District::all();
		$districts = District::paginate(10);
		$this->data = array(
			'districts' => $districts,
			'title' => 'DISTRICTS',
			'menu' => 'Other'
			);
		return View::make('districts.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('districts.create')->with('title', 'NEW DISTRICT');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), District::$rules);
		if($validator->passes()){
			$district = new District();
			$district->district_name = Input::get('district_name');
			$district->save();
			return Redirect::to('districts')
			->with('message', 'Success !! New District Created.');
		}
		return Redirect::to('districts/create')
		->withInput()
		->withErrors($validator)
		->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$district = District::find($id);
		if(!is_null($district)){
				return View::make('districts.show', $district);
		}
		return Redirect::to('/')
		->with('message', 'District is not available. Please Retry.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$district = District::find($id);
		if(is_null($district))
		{
			return Redirect::to('districts')->with('message', 'District not available. Please Retry.' );
		}
		$this->data = array(
			'title' => 'EDIT DISTRICT',
			'district' => $district
			);

		return View::make('districts.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$district = District::find($id);
		if(!is_null($district)){
		$validator = Validator::make(Input::all(), District::$rules);
			if($validator->passes()){
				$district->district_name = Input::get('district_name');
				
				$district->update();
				return Redirect::to('districts')
					->with('message', 'Success !! District updated.');
			}
			return Redirect::route('districts.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('districts')
			->with('message', 'District not available. Please Retry.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$district = District::find($id);
		if(!is_null($district)){
			$district->delete();
			return Redirect::back()
				->with('message', 'Success !! District Deleted.');
		}
		return Redirect::to('/')
			->with('message', 'District not available. Please Retry.');
	}

}