<?php

class UsersController extends BaseController {

	public function __construct(){
		$this->beforeFilter('admin', array('except' => array('getLogin', 'postLogin', 'getLogout')));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		$users = User::paginate(10);
		$this->data = array(
			'users' => $users,
			'title' => 'USERS',
			'menu' => 'Admin'
			);
		return View::make('users.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create')->with('title', 'NEW USER');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), User::$rules);

		if($validator->passes()){
			$user = new User();
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->admin = Input::get('admin');

		if(Input::file('image') != '')
		{
			$image = Input::file('image');
			$filename = time().'-'.$image->getClientOriginalName();
			$path = 'assets/img/users/';
			$img = Image::make($image->getRealPath())->resize(215, 215)->save($path.'/'.$filename);

 			$user->image = 'assets/img/users/'.$filename;
		}
			$user->save();

			return Redirect::to('users')
			->with('message', 'Success !! New User created.');
		}
		return Redirect::to('users/create')
		->withErrors($validator)
		->withInput()
		->with('message', 'Please check on the listed errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		if(!is_null($user)){
				return View::make('users.show', $user);
		}
		return Redirect::to('/')
		->with('message', 'User is not available. Please Retry.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$user = User::find($id);
		if(is_null($user))
		{
			return Redirect::to('users')->with('message', 'User not available. Please Retry.' );
		}
		$this->data = array(
			'title' => 'EDIT USER',
			'user' => $user
			);

		return View::make('users.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::find($id);
		if(!is_null($user)){
		User::$rules['email'] = 'required|email|unique:users,email,'.$id;
		$validator = Validator::make(Input::all(), User::$rules);
			if($validator->passes()){
				$user->first_name = Input::get('first_name');
				$user->last_name = Input::get('last_name');
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->admin = Input::get('admin');

			if(Input::file('image') != '')
			{
				$image = Input::file('image');
				$filename = time().'-'.$image->getClientOriginalName();
				$path = 'assets/img/users/';
				$img = Image::make($image->getRealPath())->resize(215, 215)->save($path.'/'.$filename);
	 			//delete the previous image
	 			if($image != '' && $user->image != ''){
	 				File::delete(public_path().'/'.$user->image);
	 			}
	 			$user->image = 'assets/img/users/'.$filename;
	 		}
				$user->update();
				return Redirect::to('users')
					->with('message', 'Success !! User updated.');
			}
			return Redirect::route('users.edit', $id)
			->with('message', 'Please check on the listed validation errors.')
			->withErrors($validator)
			->withInput();

			
		}
		return Redirect::to('users')
			->with('message', 'User not available. Please Retry.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		if(!is_null($user)){

			if($user->image != ''){
				File::delete(public_path().'/'.$user->image);
			}

			$user->delete();
			return Redirect::back()
				->with('message', 'Success !! User Deleted.');
		}
		return Redirect::to('/')
			->with('message', 'User not available. Please Retry.');
	}

	/**
	 * Sign the user in the system
	 */

	public function getLogin(){
		return View::make('users.login');

	}
	/**
	 * Process the signin form
	 * @return [type]
	 */
	public function postLogin(){
		$validator = Validator::make(Input::all(), User::$rules_login);
		if ($validator->passes()) {
			$details = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
			);
			
			if(Auth::attempt($details)){
				return Redirect::to('users');
			}
			return Redirect::to('login')
				->with('message',  'Invalid Email / Password');
		}
		return Redirect::to('login')
			->withErrors($validator)
			->withInput();

	}

	/**
	 * Signout the user
	 * @return [type]
	 */
	public function getLogout(){
		if (Auth::check()) {
			Auth::logout();
			return Redirect::to('login');
		}
		return Redirect::to('login');
	}
}