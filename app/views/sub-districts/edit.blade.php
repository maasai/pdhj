@extends('layouts.main')
@section('content')

<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-6">
     @if($errors->any())
<div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
{{ implode('', $errors->all(':message<br/>')) }}
</div>
        @endif
    <!-- general form elements -->
    <div class="box box-primary">
                               
                              <!-- form start -->
        {{ Form::model($subdistrict, array('method' => 'PATCH', 'files' => true, 'route' => array('sub_districts.update', $subdistrict->id)))}}

    <div class="box-body">
        <div class="form-group">              	
	        {{ Form::label('sub_district_name', 'Sub District Name') }}
	        {{ Form::text('sub_district_name', Input::old('sub_district_name'), array('placeholder'=>'Sub District Name', 'class'=>'form-control')) }}  
        </div> 
        <div class="form-group"> 
        <a href="{{ URL::to('sub_districts') }}" class="btn btn-default pull-right btn-sm"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>	             	
	      {{ Form::submit('Update Sub District', array('class'=>'btn btn-primary')) }}  
        </div>

    </div> 
       </div><!-- /.box -->
                           
               
                        </div><!--/.col (left) -->
               <!-- right column -->
                       
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop