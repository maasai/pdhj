@extends('layouts.main')
@section('content')
                <!-- Main content -->
                <section class="content">                    
                   

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-6 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Sub District Name</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
<?php $count = $subdistricts->getFrom(); ?>
    @foreach ($subdistricts as $subdistrict)
        <tr>
            <td>{{ $count}}</td>
            <td>{{ $subdistrict->sub_district_name }}</td>
            <td>
 <a href="{{ URL::to('sub_districts/'.$subdistrict->id.'/edit') }}" class="btn btn-info pull-right btn-sm"><i class="fa fa-edit"></i> Edit</a>
            </td>
            <td>
                {{ Form::open(array('method'=> 'DELETE', 'route' => array('sub_districts.destroy', $subdistrict->id))) }}
                {{Form::button('<i class="fa fa-times"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick'=>'if(!confirm("Are you sure to delete this item? This cannot be undone.")){return false;}'))}}
                {{ Form::close() }}
            </td>

        </tr>
        <?php $count++; ?>
    @endforeach
                               </tbody>
                            </table>   
                            <hr/>   
                                            <div>    
                                                
                                                 {{$subdistricts->links()}}   <a href="{{ URL::to('sub_districts/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i> New Sub District</a>
                                                 
                                             </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                 
                            </section><!-- /.content -->
                <hr>
            </aside><!-- /.right-side -->
@stop