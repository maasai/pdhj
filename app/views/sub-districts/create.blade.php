@extends('layouts.main')
@section('content')

                   <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                             @if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <!-- general form elements -->
                            <div class="box box-primary">
                               
                              <!-- form start -->
        {{ Form::open(array('route'=>'sub_districts.store')) }}
    <div class="box-body">
        <div class="form-group">              	
	        {{ Form::label('sub_district_name', 'Sub District Name') }}
	        {{ Form::text('sub_district_name', '', array('placeholder'=>'Sub District Name', 'class'=>'form-control')) }}  
        </div> 
         
        
        <div class="form-group">
        <a href="{{ URL::to('sub_districts') }}" class="btn btn-default pull-right btn-sm"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>	     
            {{ Form::submit('Save Sub District', array('class'=>'btn btn-primary btn-sm')) }}  
        </div>

    </div>       
                            </div><!-- /.box -->

               
                        </div><!--/.col (left) -->
            
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop