@extends('layouts.main')
@section('content')

                   <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                             @if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <!-- general form elements -->
                            <div class="box box-primary">
                               
                              <!-- form start -->
        {{ Form::open(array('route'=>'workshops.store')) }}
    <div class="box-body">
        <div class="form-group col-md-4">              	
	        {{ Form::label('start_date', 'Start Date') }}
	        {{ Form::text('start_date', '', array('placeholder'=>'Start Date', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group col-md-4">              	
	        {{ Form::label('end_date', 'End Date') }}
	        {{ Form::text('end_date', '', array('placeholder'=>'End Date', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group col-md-4">              	
	        {{ Form::label('total_hours', 'Total Hours') }}
	        {{ Form::text('total_hours', '', array('placeholder'=>'Total Hours', 'class'=>'form-control')) }}  
        </div>

        <div class="form-group col-md-4">               
          {{ Form::label('district', 'District') }}
          {{ Form::text('district', '', array('placeholder'=>'District', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group col-md-4">                
          {{ Form::label('sub_district', 'Sub District') }}
          {{ Form::text('sub_district', '', array('placeholder'=>'Sub District', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group col-md-4">                
          {{ Form::label('suco', 'Suco') }}
          {{ Form::text('suco', '', array('placeholder'=>'Suco', 'class'=>'form-control')) }}  
        </div>



         <div class="form-group col-md-4">               
          {{ Form::label('place', 'Specific Place') }}
          {{ Form::text('place', '', array('placeholder'=>'Specific Place', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group col-md-4">                
          {{ Form::label('institution', 'Institution') }}
          {{ Form::text('institution', '', array('placeholder'=>'Institution', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group col-md-4">                
          {{ Form::label('target_group', 'Target Group') }}
          {{ Form::text('target_group', '', array('placeholder'=>'Target Group', 'class'=>'form-control')) }}  
        </div>



         <div class="form-group col-md-6">               
            {{ Form::label('general_subject', 'General Subject') }}
            {{ Form::select('general_subject', array('1'=>'Admin', '0' => 'Not     Admin'), '0', array('class'=>'form-control')) }}  
        </div>
        <div class="form-group col-md-6">               
            {{ Form::label('specific_subject', 'Specific Subject') }}
            {{ Form::select('specific_subject', array('1'=>'Admin', '0' => 'Not     Admin'), '0', array('class'=>'form-control')) }}  
        </div>


        <div class="form-group col-md-12">               
          {{ Form::label('team', 'Facilitator\'s Team') }}
          {{ Form::textarea('team', '', array('placeholder'=>'Facilitator\'s Team', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group col-md-4">                
          {{ Form::label('females', 'Females Participants') }}
          {{ Form::text('females', '', array('placeholder'=>'Females Participants', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group col-md-4">                
          {{ Form::label('males', 'Male Participants') }}
          {{ Form::text('males', '', array('placeholder'=>'Male Participants', 'class'=>'form-control')) }}  
        </div>
        <div class="form-group col-md-4">                
          {{ Form::label('total_participants', 'Total Participants') }}
          {{ Form::text('total_participants', '', array('placeholder'=>'Total Participants', 'class'=>'form-control')) }}  
        </div>
    
    <hr>

         
        <div class="form-group col-md-12">
        <a href="{{ URL::to('workshops') }}" class="btn btn-default pull-right btn-sm"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>	     
            {{ Form::submit('Save WorkShop', array('class'=>'btn btn-primary btn-sm')) }}  
        </div>

    </div>       
                            </div><!-- /.box -->

               
                        </div><!--/.col (left) -->
            
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop