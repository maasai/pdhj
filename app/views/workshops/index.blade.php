@extends('layouts.main')
@section('content')
                <!-- Main content -->
                <section class="content">                    
                   

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Hours</th>
                                        <th>District</th>
                                        <th>Sub District</th>
                                        <th>Suco</th>
                                        <th>Place</th>
                                        <th>Institution</th>
                                        <th>Target Group</th>
                                        <th>General Subject</th>
                                        <th>Specific Subject</th>
                                        <th>Team</th>
                                        <th>Males</th>
                                        <th>Females</th>
                                        <th>Added On</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
<?php $count = $workshops->getFrom(); ?>
    @foreach ($workshops as $workshop)
        <tr>
            <td>{{ $count}}</td>
            <td>{{ $workshop->start_date }}</td>
            <td>{{ $workshop->end_date }}</td>
            <td>{{ $workshop->total_hours }}</td>
            <td>{{ $workshop->district }}</td>
            <td>{{ $workshop->sub_district }}</td>
            <td>{{ $workshop->suco }}</td>
            <td>{{ $workshop->place }}</td>
            <td>{{ $workshop->institution }}</td>
            <td>{{ $workshop->target_group }}</td>
            <td>{{ $workshop->general_subject }}</td>
            <td>{{ $workshop->specific_subject }}</td>
            <td>{{ $workshop->team }}</td>
            <td>{{ $workshop->males }}</td>
            <td>{{ $workshop->females }}</td>
           <td>{{ $workshop->created_at }}</td>
            <td>
 <a href="{{ URL::to('workshops/'.$workshop->id.'/edit') }}" class="btn btn-info pull-right btn-sm"><i class="fa fa-edit"></i> Edit</a>
            </td>
            <td>
                {{ Form::open(array('method'=> 'DELETE', 'route' => array('workshops.destroy', $workshop->id))) }}
                {{Form::button('<i class="fa fa-times"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick'=>'if(!confirm("Are you sure to delete this item? This cannot be undone.")){return false;}'))}}
                {{ Form::close() }}
            </td>

        </tr>
        <?php $count++; ?>
    @endforeach
                               </tbody>
                            </table>   
                            <hr/>   
                                            <div>    
                                                
                                                 <a href="{{ URL::to('workshops/create') }}" class="btn btn-success pull-right"><i class="fa fa-user"></i> Create New Workshop</a>
                                                  {{$workshops->links()}}  
                                             </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                 
                            </section><!-- /.content -->
                <hr>
            </aside><!-- /.right-side -->
@stop