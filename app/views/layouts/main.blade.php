<!DOCTYPE html>
<html>
    <head>
        @include('components.head')
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            @include('components.header')
        </header>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
    @include('components.sidebar')
            <!-- Page content start-->
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                       <h4>
                         {{ "PDHJ SYTEM" }}
                       @if(isset($title))
                           - {{ $title }}
                                                 
                        @endif
                        </h4>
                                         
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Examples</a></li>
                        <li class="active">Blank page</li>
                    </ol>
                </section>
            @if (Session::has('message'))
                <div class="pad margin no-print">
                     <div class="alert alert-info" style="margin-bottom: 0!important;">
                        <i class="fa fa-info"></i>
                        {{ Session::get('message')}}
                    </div>
                </div>
            @endif
 
    @yield('content')

              <!-- Page content end-->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


     @include('components.footer')

    </body>
</html>