@extends('layouts.main')
@section('content')
                <!-- Main content -->
                <section class="content">                    
                   

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th></th>
                                        <th>Added On</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
<?php $count = $users->getFrom(); ?>
    @foreach ($users as $user)
        <tr>
            <td>{{ $count}}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->image != '')
                {{ HTML::image($user->image, $user->first_name, array('class' => 'prof-pic')) }}
                @endif

            </td>
            <td>{{ $user->created_at }}</td>
            <td>
 <a href="{{ URL::to('users/'.$user->id.'/edit') }}" class="btn btn-info pull-right btn-sm"><i class="fa fa-edit"></i> Edit</a>
            </td>
            <td>
                {{ Form::open(array('method'=> 'DELETE', 'route' => array('users.destroy', $user->id))) }}
                {{Form::button('<i class="fa fa-times"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick'=>'if(!confirm("Are you sure to delete this item? This cannot be undone.")){return false;}'))}}
                {{ Form::close() }}
            </td>

        </tr>
        <?php $count++; ?>
    @endforeach
                               </tbody>
                            </table>   
                            <hr/>   
                                            <div>    
                                                
                                                 <a href="{{ URL::to('users/create') }}" class="btn btn-success pull-right"><i class="fa fa-user"></i> Create New User</a>
                                                  {{$users->links()}}  
                                             </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                 
                            </section><!-- /.content -->
                <hr>
            </aside><!-- /.right-side -->
@stop