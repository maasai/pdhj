@extends('layouts.main')
@section('content')

<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-6">
     @if($errors->any())
<div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
{{ implode('', $errors->all(':message<br/>')) }}
</div>
        @endif
    <!-- general form elements -->
    <div class="box box-primary">
                               
                              <!-- form start -->
        {{ Form::model($user, array('method' => 'PATCH', 'files' => true, 'route' => array('users.update', $user->id)))}}

    <div class="box-body">
        <div class="form-group">              	
	        {{ Form::label('first_name', 'First Name') }}
	        {{ Form::text('first_name', Input::old('first_name'), array('placeholder'=>'User First Name', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group">              	
	        {{ Form::label('last_name', 'Last Name') }}
	        {{ Form::text('last_name', Input::old('last_name'), array('placeholder'=>'User Last Name', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group">              	
	        {{ Form::label('email', 'Email Address') }}
	        {{ Form::text('email', Input::old('email'), array('placeholder'=>'User Email Address', 'class'=>'form-control')) }}  
        </div>
          <div class="form-group">              	
	        {{ Form::label('admin', 'User Role') }}
	        {{ Form::select('admin', array('1'=>'Admin', '0' => 'Not     Admin'), $user->admin, array('class'=>'form-control')) }}  
        </div>
         <div class="form-group">               
            {{ Form::label('image', 'New User Photo (Will replace the current photo)') }}
            {{ Form::file('image') }}
        </div>
         <div class="form-group">               
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control')) }}
        </div>
           <div class="form-group">               
            {{ Form::label('password_confirmation', 'Confirm Password') }}
            {{ Form::password('password_confirmation',array('placeholder'=>'Confirm Password', 'class'=>'form-control')) }}
        </div>
        <div class="form-group"> 
        <a href="{{ URL::to('users') }}" class="btn btn-default pull-right btn-sm"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>	             	
	      {{ Form::submit('Update User', array('class'=>'btn btn-primary')) }}  
        </div>

    </div> 
       </div><!-- /.box -->
                           
               
                        </div><!--/.col (left) -->
               <!-- right column -->
                        <div class="col-md-6">
                            <!-- general form elements disabled -->                                
                                <div class="box-body">
                                	@if($user->image != '')
                                	<h4>{{ "Profile Image :  ". ucfirst($user->first_name)  }}</h4>
         {{ HTML::image($user->image, $user->first_name, array('class' => 'user-header')) }}
         @endif
                                </div><!-- /.box-body -->
                           
                        </div><!--/.col (right) -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop