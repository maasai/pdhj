<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>PDHJ System | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {{ HTML::style('assets/css/bootstrap.min.css')}}
        <!-- font Awesome -->
        {{ HTML::style('assets/css/font-awesome.min.css')}}
        <!-- Theme style -->
        {{ HTML::style('assets/css/AdminLTE.css')}}
         {{ HTML::style('assets/css/custom.css')}}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Sign In</div>
<!--            {{ '<pre>'.print_r(Session::all()).'</pre>' }}
 --> 

 @if(Session::has('message'))
 <h5  class="alert alert-danger">{{ Session::get('message') }}</h5>
 @endif

 <div id="validation-errors" class="alert alert-danger"></div>
 @if($errors->any())
  <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
   {{ implode('', $errors->all(':message<br/>')) }}
    </div>
          @endif


    {{ Form::open(array('url' => 'login'))}}
                <div class="body bg-gray">
                              <div class="form-group">
                        {{Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Your Email'))}}
                        </div>
                         <div class="form-group">
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                          <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                            </div>
                               </div>
                 <div class="footer">                                                               
                    {{Form::submit('Sign me in', array('class' => 'btn bg-olive btn-block'))}} 
                    <div id="div-loading">
                        <span id="loading" class="">
                        {{ HTML::image('assets/img/loader.gif', 'Cars') }}</span> 
                    </div>
                    <p><a href="#">I forgot my password</a></p>
                 
                </div>
                {{form::close()}}
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        {{HTML::script('assets/js/bootstrap.min.js')}}    
        {{HTML::script('assets/js/custom-js.js')}}     

    </body>
</html>


