@extends('layouts.main')
@section('content')

                   <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                             @if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <!-- general form elements -->
                            <div class="box box-primary">
                               
                              <!-- form start -->
        {{ Form::open(array('route'=>'users.store', 'files' => true)) }}
    <div class="box-body">
        <div class="form-group">              	
	        {{ Form::label('first_name', 'First Name') }}
	        {{ Form::text('first_name', '', array('placeholder'=>'First Name', 'class'=>'form-control')) }}  
        </div> 
         <div class="form-group">              	
	        {{ Form::label('last_name', 'Last Name') }}
	        {{ Form::text('last_name', '', array('placeholder'=>'Last Name', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group">              	
	        {{ Form::label('email', 'Email Address') }}
	        {{ Form::text('email', '', array('placeholder'=>'Email Address', 'class'=>'form-control')) }}  
        </div>
         <div class="form-group">               
            {{ Form::label('admin', 'Is Admin') }}
            {{ Form::select('admin', array('1'=>'Admin', '0' => 'Not     Admin'), '0', array('class'=>'form-control')) }}  
        </div>
         <div class="form-group">               
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control')) }}
        </div>
           <div class="form-group">               
            {{ Form::label('password_confirmation', 'Confirm Password') }}
            {{ Form::password('password_confirmation',array('placeholder'=>'Confirm Password', 'class'=>'form-control')) }}
        </div>
        <div class="form-group">               
            {{ Form::label('image', 'User Photo') }}
            {{ Form::file('image') }}
        </div>
        <div class="form-group">
        <a href="{{ URL::to('users') }}" class="btn btn-default pull-right btn-sm"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>	     
            {{ Form::submit('Save User', array('class'=>'btn btn-primary btn-sm')) }}  
        </div>

    </div>       
                            </div><!-- /.box -->

               
                        </div><!--/.col (left) -->
            
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop