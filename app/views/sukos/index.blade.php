@extends('layouts.main')
@section('content')
                <!-- Main content -->
                <section class="content">                    
                   

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-6 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Suko Name</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
<?php $count = $sukos->getFrom(); ?>
    @foreach ($sukos as $suko)
        <tr>
            <td>{{ $count}}</td>
            <td>{{ $suko->suko_name }}</td>
            <td>
 <a href="{{ URL::to('sukos/'.$suko->id.'/edit') }}" class="btn btn-info pull-right btn-sm"><i class="fa fa-edit"></i> Edit</a>
            </td>
            <td>
                {{ Form::open(array('method'=> 'DELETE', 'route' => array('sukos.destroy', $suko->id))) }}
                {{Form::button('<i class="fa fa-times"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick'=>'if(!confirm("Are you sure to delete this item? This cannot be undone.")){return false;}'))}}
                {{ Form::close() }}
            </td>

        </tr>
        <?php $count++; ?>
    @endforeach
                               </tbody>
                            </table>   
                            <hr/>   
                        </div><!-- /.col -->
                        <div  class="col-xs-9 table-responsive"> {{$sukos->links()}}
<a href="{{ URL::to('sukos/create') }}" class="btn btn-success pull-left"><i class="fa fa-plus"></i> New Suko</a>

                         </div>
                    </div><!-- /.row -->
                 
                            </section><!-- /.content -->
                <hr>
            </aside><!-- /.right-side -->
@stop