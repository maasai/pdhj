@extends('layouts.main')
@section('content')

                   <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                             @if($errors->any())
                 <div class="alert alert-danger alert-dismissable  alert alert-danger alert-dismissable">
                        {{ implode('', $errors->all(':message<br/>')) }}
                     </div>
                                @endif
                            <!-- general form elements -->
                            <div class="box box-primary">
                               
                              <!-- form start -->
        {{ Form::open(array('route'=>'sukos.store')) }}
    <div class="box-body">
        <div class="form-group">              	
	        {{ Form::label('suko_name', 'Suko Name') }}
	        {{ Form::text('suko_name', '', array('placeholder'=>'Suko Name', 'class'=>'form-control')) }}  
        </div> 
         
        
        <div class="form-group">
        <a href="{{ URL::to('sukos') }}" class="btn btn-default pull-right btn-sm"><i class="glyphicon glyphicon-chevron-left"></i> Cancel</a>	     
            {{ Form::submit('Save Suko', array('class'=>'btn btn-primary btn-sm')) }}  
        </div>

    </div>       
                            </div><!-- /.box -->

               
                        </div><!--/.col (left) -->
            
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop