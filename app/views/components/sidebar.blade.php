            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                    @if(Auth::check())
                        @if(Auth::user()->image != '')
{{ HTML::image(Auth::user()->image, Auth::user()->first_name, array('class'=>'img-circle'))}}
                        @endif
                     
                     @endif
                        </div>
                        <div class="pull-left info">
                             @if(Auth::check())
                            <p>Hello, {{ Auth::user()->first_name }}</p>
                 
                        @endif

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li
                         @if(isset($menu) && $menu == 'Dashboard')
                         {{ 'class="active"' }} 
                         @endif
                          >
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-th"></i> <span>Activities</span> 
                            </a>
                        </li>
                         <li
                          @if(isset($menu) && $menu == 'WorkShop')
                             {{ 'class="active"' }} 
                         @endif
                         >
                            <a href="{{ URL::to('workshops') }}">
                                <i class="fa fa-th"></i> <span>Workshop</span> 
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Materials</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Morris</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Flot</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Inline charts</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>Monitoring</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> General</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Icons</a></li>
                                                        
                            </ul>
                        </li>
                     
                        <li
                         @if(isset($menu) && $menu == 'Admin')
                         {{ 'class="active treeview"' }} 
                         @endif

                         class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Admin Users</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('users') }}">
                                    <i class="fa fa-angle-double-right"></i> All Users</a></li>
                                <li><a href="{{ URL::to('users/create') }}"><i class="fa fa-angle-double-right"></i> New User</a></li>
                            </ul>
                        </li>
                           <li
                           @if(isset($menu) && $menu == 'Other')
                         {{ 'class="active treeview"' }} 
                         @endif
                            class="treeview">
                            <a href="#">
                                <i class="fa fa-cog"></i> <span>Other Data</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('districts') }}"><i class="fa fa-angle-double-right"></i> Districts</a></li>
                                <li><a href="{{ URL::to('sub_districts') }}"><i class="fa fa-angle-double-right"></i> Sub Districts</a></li>
                                <li><a href="{{ URL::to('sukos') }}"><i class="fa fa-angle-double-right"></i> Suko</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>