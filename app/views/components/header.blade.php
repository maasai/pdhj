<a href="{{ URL::to('users') }}" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                PDHJ SYSTEM
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>
                                    @if(Auth::check())
                                    {{ Auth::user()->first_name }}
                              
                                @endif
                                <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                     @if(Auth::check())
                                        @if(Auth::user()->image != '')
{{ HTML::image(Auth::user()->image, Auth::user()->first_name, array('class'=>'img-circle'))}}
                                        @endif
                                    <p>
                                        {{ ucfirst(Auth::user()->first_name) }}
                                    </p>
                                @endif
                                </li>
                           
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                @if(Auth::check())
{{link_to_route('users.edit', 'Profile', Auth::user()->id, array('class' => 'btn btn-default btn-flat')) }}                      @endif
                                  </div>
                                    <div class="pull-right">
                {{ HTML::link('logout', 'Sign out', array('class'=>'btn btn-default btn-flat'))}}
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>