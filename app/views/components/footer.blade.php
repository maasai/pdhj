<!-- jQuery 2.0.2 -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        {{ HTML::script('assets/js/jquery-ui-1.10.3.min.js') }}
        <!-- Bootstrap -->
        {{ HTML::script('assets/js/bootstrap.min.js') }}
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        {{ HTML::script('assets/js/plugins/morris/morris.min.js') }}   
        <!-- Sparkline -->
        {{ HTML::script('assets/js/plugins/sparkline/jquery.sparkline.min.js') }}   
        <!-- jvectormap -->
        {{ HTML::script('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}   
        {{ HTML::script('assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}   
        <!-- fullCalendar -->
        {{ HTML::script('assets/js/plugins/fullcalendar/fullcalendar.min.js') }}   
        <!-- jQuery Knob Chart -->
        {{ HTML::script('assets/js/plugins/jqueryKnob/jquery.knob.js') }}   
        <!-- daterangepicker -->
        {{ HTML::script('assets/js/plugins/daterangepicker/daterangepicker.js') }}   
        <!-- Bootstrap WYSIHTML5 -->
        {{ HTML::script('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}   
        <!-- iCheck -->
        {{ HTML::script('assets/js/plugins/iCheck/icheck.min.js') }}   

        <!-- AdminLTE App -->
        {{ HTML::script('assets/js/AdminLTE/app.js') }}   
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
         {{ HTML::script('assets/js/AdminLTE/dashboard.js') }}   