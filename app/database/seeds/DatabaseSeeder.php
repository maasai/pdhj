<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
public function run()
	{
		$user = new User();
		$user->first_name = 'Jon';
		$user->last_name = 'Doe';
		$user->email = 'admin@admin.com';
		$user->password = Hash::make('admin');
		$user->admin = 1;
		$user->save();
		
	}

}
