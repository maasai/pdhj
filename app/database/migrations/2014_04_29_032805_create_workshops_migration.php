<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopsMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('work_shops', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('start_date');
			$table->dateTime('end_date');
			$table->string('total_hours');
			$table->integer('district_id');
			$table->integer('sub_district_id');
			$table->integer('suco_id');
			$table->string('place');
			$table->string('institution');
			$table->string('target_group');
			$table->string('general_subject');
			$table->string('specific_subject');
			$table->string('facilitator_team');
			$table->string('males');
			$table->string('females');			
			$table->string('total_participants');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('work_shops');
	}

}
