<?php
class WorkShop extends Eloquent {

public static $rules = array(
	'start_date' => 'required',
	'end_date' => 'required',
	'total_hours' => 'required',
	'district' => 'required',
	'sub_district' => 'required',
	'suco' => 'required',
	'place' => 'required',
	'institution' => 'required',
	'target_group' => 'required',
	'general_subject' => 'required',
	'specific_subject' => 'required',
	'team' => 'required',
	'males' => 'required',
	'females' => 'required',
	'total_participants' => 'required'
	);
}