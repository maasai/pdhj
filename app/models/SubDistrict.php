<?php

class SubDistrict extends \Eloquent {
	protected $fillable = ['sub_district_name'];

	public static $rules = array(
		'sub_district_name' => 'required'
		);
}