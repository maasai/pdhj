<?php

class District extends Eloquent {
	protected $fillable = ['district_name'];

	public static $rules  = array(
		'district_name' => 'required'
		);
}