<?php
class Suko extends Eloquent {
	protected $fillable = ['suko_name'];

	public static $rules = array(
		'suko_name' => 'required'
		);
}