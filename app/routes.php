<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('login', 'UsersController@getLogin');
Route::get('logout', 'UsersController@getLogout');
Route::post('login', 'UsersController@postLogin');
Route::resource('users', 'UsersController');
Route::resource('workshops', 'WorkShopsController');
Route::resource('districts', 'DistrictsController');
Route::resource('sub_districts', 'SubDistrictsController');
Route::resource('sukos', 'SukosController');
Route::get('/', 'HomeController@showWelcome');
